#!/usr/bin/env python3

'''Split m4a file by chapters and convert to mp3'''

# Copyright (c) 2019 Ruslan Mstoi <ruslan.mstoi@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import re
import sys
import json
import shutil
import shlex
import argparse
import subprocess


def parse_args():
    """Parse command line arguments and options"""
    arg_parser = argparse.ArgumentParser(description=__doc__.splitlines()[0])

    arg_parser.add_argument("-f", "--files", nargs='+',
                            help="m4a files to convert to mp3")

    arg_parser.add_argument("-m", "--name",
                            help="Prefix to use for the names of file and "
                            "the name of directory to create. If this "
                            "argument is not specified the name will be "
                            "auto-generated from the source file name")

    arg_parser.add_argument("-e", "--no-episode",
                            action='store_true',
                            help="Do not append to the 'name' argument the "
                            "episode number found in the source file name. "
                            "This argument is only used if the 'name' "
                            "argument is specified.")

    arg_parser.add_argument("-n", "--dry-run",
                            action='store_true',
                            help="Only print information. Don't generate mp3 "
                            "files")
    args = arg_parser.parse_args()

    if not args.files:
        print("Please specify some files!")
        arg_parser.print_help()
        sys.exit(5)

    return args


# TODO: way too many arguments
def gen_chapter_file(in_file, title, index, total, start, end, dry_run):
    """"Make the mp3 file

    in_file -- original file
    index -- track index
    total -- total number of tracks
    title -- title of file
    start -- beginning of file
    end -- end of file
    dry_run -- only print information. Don't generate mp3 files
    """

    duration = end - start
    out_file = "{}-{:03d}-{:03d}.mp3".format(title, index, total)

    if os.path.isfile(out_file):
        print("File %s exist, skipping" % out_file)
        return

    cmd = 'ffmpeg -i "{}" -ss {} -t {} "{}"'.format(in_file, start, duration,
                                                    out_file)
    print("Running: {}".format(cmd))
    if not dry_run:
        subprocess.run(shlex.split(cmd), check=True, universal_newlines=True)


def get_uiname(m4a_file, args):
    """Return music player UI friendly file name

    On mobile devices UI space is limited, so it is a good idea to keep the
    names short

    m4a_file -- name of m4a_file
    args -- command line arguments

    """
    if args.name:
        uiname = args.name

        if not args.no_episode:
            episode = re.search(r"\d+", m4a_file).group(0)
            if episode:
                uiname += "-" + episode

    else:  # try to generate from source name
        uiname = m4a_file.lower().replace(" ", "-")

        # replace dots within name - excluding in directory and extension
        dirname = os.path.dirname(m4a_file)
        uiname = os.path.basename(uiname)
        uiname, ext = os.path.splitext(uiname)
        uiname = uiname.replace(".", "")
        uiname = os.path.join(dirname, uiname)

    return uiname


def do_convert(m4a_file, args):
    if not os.path.splitext(m4a_file)[1] == ".m4a":
        print("Skipping %s, not m4a file" % m4a_file)

    uiname = get_uiname(m4a_file, args)
    dirname = uiname
    chdir_done = False
    print("Destination directory: {}".format(dirname))

    if not args.dry_run:
        if not os.path.isdir(dirname):
            os.mkdir(dirname)
        os.chdir(dirname)
        chdir_done = True
        m4a_file = os.path.join("..", os.path.split(m4a_file)[1])

    cmd = \
        'ffprobe -loglevel quiet -show_chapters -of json "{}"'.format(m4a_file)
    print("Running: {}".format(cmd))
    proc = subprocess.run(shlex.split(cmd), check=True, stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT, universal_newlines=True)

    json_ffmpeg = json.loads(proc.stdout)
    json_chapters = json_ffmpeg['chapters']

    for chapter in json_chapters:
        print(chapter)
        ch_id = chapter["id"]
        ch_start = float(chapter["start_time"])
        ch_end = float(chapter["end_time"])
        print("id={} start={}, end={}".format(ch_id, ch_start, ch_end))
        gen_chapter_file(m4a_file, dirname, ch_id + 1, len(json_chapters),
                         ch_start, ch_end, args.dry_run)

    if chdir_done:
        os.chdir("..")


def main():
    """Main."""
    args = parse_args()

    for m4a_file in args.files:
        do_convert(m4a_file, args)


if __name__ == "__main__":
    main()
